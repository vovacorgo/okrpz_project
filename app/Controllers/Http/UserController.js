'use strict'

const { session } = require('../../../config/auth');

const User = use('App/Models/User');
const {validate} = use('Validator');

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with users
 */
class UserController {
  /**
   * Show a list of all users.
   * GET users
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view }) {
  }

  /**
   * Render a form to be used for entering user info
   * GET enter
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async enter ({ request, response, view }) {
    return view.render('enter');
  }

  /**
   * Check if user exists and authorize.
   * POST enter
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async login ({ auth, request, response, session }) {
    const {email, password} = request.all();
    const rules = {email: 'required', password: 'required'}
    var validation = await validate(request.all(), rules);
    if(validation.fails()){
      session.withErrors(validation.messages()).flashAll()
      return response.redirect('back')
    }
    try{
      await auth.attempt(email, password);
    } catch(err) {
      session.withErrors({error: err}).flashOnly('error')
      return response.redirect('back')
    }
    return response.redirect('/');
  }

  /**
   * Render a form to be used for registering a new user.
   * GET register
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async registration ({ view }) {
    
    return view.render('registration');
  }

  /**
   * Register/save a new user.
   * POST register
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response, session }) {
    const {email, password, confirmpassword} = request.all();
    const rules = {email: 'required', password: 'required', confirmpassword: 'required'};

    var validation = await validate(request.all(), rules);
    if(validation.fails()){
      session.withErrors(validation.messages()).flashAll();
      return response.redirect('back');
    }

    if (password !== confirmpassword){
      session.withErrors({password: "Passwords are not equal"}).flashAll();
      return response.redirect('back');
    }

    if(await User.findBy({email: email})){
      session.withErrors({email: "There is such user"}).flashAll();
      return response.redirect('back');
    }

    var user = new User();
    user.email = email;
    user.password = password;
    user.save();
    
    return response.redirect('/enter');
  }

  /**
   * Render a form to update an existing user.
   * GET users/:id/edit
   * 
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, request, response, view }) {
  }

  /**
   * Update user details.
   * PUT or PATCH users/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
  }

  /**
   * Logoff the user.
   * GET exit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async exit ({ auth, response }) {
    await auth.logout();
    return response.redirect('/enter');
  }
  
  /**
   * Delete a user with id.
   * DELETE users/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, response }) {
  }
}

module.exports = UserController
