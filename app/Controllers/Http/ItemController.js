'use strict'

const User = use('App/Models/User');
const Item = use('App/Models/ListItem');

/** @typedef {import('@adonisjs/framework/src/Request')} Request */
/** @typedef {import('@adonisjs/framework/src/Response')} Response */
/** @typedef {import('@adonisjs/framework/src/View')} View */

/**
 * Resourceful controller for interacting with items
 */
class ItemController {
  /**
   * Show a list of all items.
   * GET items
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async index ({ request, response, view, auth }) {
    var items = await Item
      .query()
      .where('user_id', auth.user.id)
      .fetch();
    return view.render('index', {items: items.toJSON()});
  }

  /**
   * Render a form to be used for creating a new item.
   * GET items/create
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async create ({ request, response, view }) {
    return view.render('add');
  }

  /**
   * Create/save a new item.
   * POST items
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async store ({ request, response, auth }) {
    const {content} = request.all();
    var item = new Item();
    item.content = content;
    item.user_id = auth.user.id;
    item.save();
    return response.redirect('/');
  }

  /**
   * Display a single item.
   * GET items/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async show ({ params, request, response, view }) {
  }

  /**
   * Render a form to update an existing item.
   * GET items/:id/edit
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   * @param {View} ctx.view
   */
  async edit ({ params, response, view }) {
    const {id} = params
    const item = await Item.find(id)
    if(!item){
      return response.redirect('back');
    }
    return view.render('edit', {item: item});
  }

  /**
   * Update item details.
   * PUT or PATCH items/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async update ({ params, request, response }) {
    const {id} = params
    const {content, marked} = request.all()
    await Item
      .query()
      .where('id', id)
      .update({ 
        content: content, 
        marked: !!marked 
      })
    return response.redirect('/')
  }

  /**
   * Delete a item with id.
   * DELETE items/:id
   *
   * @param {object} ctx
   * @param {Request} ctx.request
   * @param {Response} ctx.response
   */
  async destroy ({ params, request, auth, response }) {
    const {id} = params
    const item = await Item.find(id)
    if(item.user_id !== auth.user.id)
      return response.redirect('back')
    await item.delete()
    return response.redirect('/')
  }
}

module.exports = ItemController
