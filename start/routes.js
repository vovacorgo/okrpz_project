'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

// Route.on('/').render('index')
Route.get('/', 'ItemController.index').middleware('auth')

Route.get('add', 'ItemController.create').middleware('auth');
Route.post('add', 'ItemController.store').middleware('auth');

Route.get('edit/:id', 'ItemController.edit').middleware('auth');
Route.patch(':id', 'ItemController.update').middleware('auth');
Route.delete(':id', 'ItemController.destroy').middleware('auth')

Route.get('enter', 'UserController.enter').middleware('logout');
Route.post('enter', 'UserController.login');

Route.get('signup', 'UserController.registration').middleware('logout');
Route.post('signup', 'UserController.store');


Route.get('exit', 'UserController.exit').middleware('auth');