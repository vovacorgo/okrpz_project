'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ListItemSchema extends Schema {
  up () {
    this.create('list_items', (table) => {
      table.increments('id')
      table.text('content').notNullable()
      table.boolean('marked').notNullable().defaultTo(false);
      table.integer('user_id').notNullable().references('id').inTable('users')
      table.timestamps()
    })
  }

  down () {
    this.drop('list_items')
  }
}

module.exports = ListItemSchema
